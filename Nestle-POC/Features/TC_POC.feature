﻿Feature: TC_POC
	As a user
	I want to search for the cheapest 'Python For Dummies' book 
	in Amazon Drive Cloud website

@mytag
Scenario: TC_01 - Search for the cheapest book
	Given I navigate to the Amazon Drive site
	And I search for my book
	When I select the cheaper book from the list
	And I select the book type
	And I add my book to the shopping cart
	Then my book must be added to the shipping cart
