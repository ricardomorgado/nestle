﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nestle_POC
{
    public class TestConstants
    {
        public const int TestTimeout = 10;
        public const string WebSiteURL = "https://www.amazon.com/clouddrive";
        public const string BookName = "C# For Dummies";
    }

    public class ErrorMessages
    {
        public const string WebDriverInitFail = "Could not initialize the web driver because: ";
        public const string WebSiteNavFail = "Could not navigate to the website because: ";
        public const string PageNavFail = "Could not open to the ";
        public const string SearchElementInPageFail = "Could not find the {ELEMENT} in {PAGE}";
        public const string ClickElementInPageFail = "Could not click in the {ELEMENT} in {PAGE}";
        public const string NoArticlesPageFail = "Could not find any article in {PAGE}";
        public const string AddArticleToShoopingCardFail = "The article {ART} is not added to the shooping card";
    }
}
