﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

namespace Nestle_POC.Helpers
{
    class SeleniumActions
    {

        /// <summary>
        /// Wait until to the element is visible
        /// </summary>
        /// <param name="elementXpath">Element Xpath to search</param>
        /// <returns>true if the element is founded; otherwise, returns false</returns>
        public static bool WaitUntilWebElementIsVisible(string elementXpath)
        {
            // Search for the current element in the web driver for a certain
            // seconds and, then, validate if the element was found
            try
            {
                var wait = new WebDriverWait(WebDriver.Driver, TimeSpan.FromSeconds(TestConstants.TestTimeout));
                wait.Until(drv => (WebDriver.Driver.FindElements(By.XPath(elementXpath)).Count > 0) ? WebDriver.Driver.FindElements(By.XPath(elementXpath)) : null);
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Search for an element in real time (without waiting)
        /// </summary>
        /// <param name="elementXpath">Element Xpath to search</param>
        /// <returns>true if the element is founded; otherwise, returns false</returns>
        public static bool SearchElement(string elementXpath)
        {
            try
            {
                WebDriver.Driver.FindElement(By.XPath(elementXpath));
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Write text input in an element
        /// </summary>
        /// <param name="elementXpath">Xpath element to input text</param>
        /// <param name="text">Text value to be inserted</param>
        public static void InsertTextInElement(string elementXpath, string text)
        {
            // Create an IWebElement accordingly with the received XPath and
            // insert the received text over it
            IWebElement txtSearchField = WebDriver.Driver.FindElement(By.XPath(elementXpath));
            txtSearchField.SendKeys(text);
        }

        /// <summary>
        /// Click over an element
        /// </summary>
        /// <param name="elementXpath">XPath element to be clicked</param>
        /// <returns>true if the process is not fail; otherwise, returns false</returns>
        public static bool ClickInElement(string elementXpath)
        {
            // Create an IWebElement accordingly with the received XPath and
            // click on it
            try
            {
                IWebElement element = WebDriver.Driver.FindElement(By.XPath(elementXpath));
                element.Click();
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Search for the article name in the search list and click on is link
        /// </summary>
        /// <param name="elementXpath">Search list Xpath</param>
        /// <param name="articleName">Article name in the search list</param>
        /// <returns>true if the process is not fail; otherwise, returns false</returns>
        public static bool SearchAndClickInArticleLink(string elementXpath, string articleName)
        {

            // Process for searching and click over the article name in the search list:
            // - Wait 3 seconds for the list can be loaded until the end
            // - Get the string size of the article name
            // - Pass the list received from the web driver to the IWebList
            // - For each IWebList line, check if the line row text is equal to the article name
            //   - Use the 'substring' property to get the line row text
            //   - If the article name is found, click over is link text on the list; otherwise, the
            //     article was not found and the process fails

            System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(3000));
            try
            {
                int articleTitleSize = articleName.Length;
                IList<IWebElement> displayedList = WebDriver.Driver.FindElements(By.XPath(elementXpath));
                foreach (var listRow in displayedList)
                {
                    string txt = listRow.Text.Substring(0, articleTitleSize);
                    if (txt == articleName)
                    {

                        IWebElement bookLinkInList = WebDriver.Driver.FindElements(By.PartialLinkText(articleName))[1];
                        bookLinkInList.Click();
                        break;
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

    }
}
