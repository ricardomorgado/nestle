﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace Nestle_POC.Helpers
{
    public static class WebDriver
    {
        // Global web driver object
        public static IWebDriver Driver;

        /// <summary>
        /// Initialize the web driver
        /// </summary>
        /// <param name="errorMsg">Error message if the process fails</param>
        /// <returns>true if the web driver is launched; otherwise, returns false</returns>
        public static bool InitializeWebDriver(out string errorMsg)
        {
            // Initialize the 'chrome' web driver and maximize the current browser
            // window after launching it
            try
            {
                ChromeOptions options = new ChromeOptions();
                Driver = new ChromeDriver(options);
                Driver.Manage().Window.Maximize();
            }
            catch (Exception ex)
            {
                errorMsg = ErrorMessages.WebDriverInitFail + ex.Message;
                return false;
            }
            errorMsg = null;
            return true;
        }

        /// <summary>
        /// Navigate for the URL in the browser web driver
        /// </summary>
        /// <param name="url">URL to navigate</param>
        /// <param name="errorMsg">Error message if the process fails</param>
        /// <returns>true if the ULR is opened; otherwise, returns false</returns>
        public static bool NavigateToWebSite(string url, out string errorMsg)
        {
            try
            {
                Driver.Navigate().GoToUrl(url);
            }
            catch (Exception ex)
            {
                errorMsg = ErrorMessages.WebSiteNavFail + ex.Message;
                return false;
            }
            errorMsg = null;
            return true;
        }

        /// <summary>
        /// Close the web driver
        /// </summary>
        public static void CloseWebDriver()
        {
            Driver.Quit();
        }

        /// <summary>
        /// Take a snapshot to the current browser web driver
        /// </summary>
        public static void TakeScreenshot()
        {

            // If the failure was before the driver was setup then ignore
            if (Driver == null)
                return;

            // Get path to screenshots
            string screenshotPath = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName + "\\Nestle-POC\\Screenshots\\" + FeatureContext.Current.FeatureInfo.Title + "_" + TestContext.CurrentContext.Test.Name.Replace("\"", "").Replace(",System.String[]", "") + "\\";

            // Create folder if it does not exists
            bool exists = System.IO.Directory.Exists(screenshotPath);
            if (!exists)
                System.IO.Directory.CreateDirectory(screenshotPath);

            // Define name of image
            string imageFileName = DateTime.Now.ToString("yyyy-MM-dd_HH\"h\"mm\"m\"ss\"s\"") + ".png";

            // Request screenshot to driver
            Screenshot ss = ((ITakesScreenshot)Driver).GetScreenshot();

            // Save to file
            ss.SaveAsFile(screenshotPath + imageFileName, OpenQA.Selenium.ScreenshotImageFormat.Png);
            Console.WriteLine("Screenshot save into file with name " + imageFileName);

        }

    }
}
