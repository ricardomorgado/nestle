﻿using Nestle_POC.Helpers;
using Nestle_POC.Pages;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Nestle_POC
{
    [Binding]
    public sealed class TCSpecs_POC
    {

        #region Objects and private variables of the current class
        
        // Objects instantiated 
        InitialPage initPage = new InitialPage();
        ListPage listPage = new ListPage();
        ArticlePage articlePage = new ArticlePage();
        
        // Private string for error message
        private string errorMessage = null;

        #endregion

        [BeforeScenario]
        public void LaunchWebDriver()
        {
            // Initialize the Chrome WebDriver
            Assert.IsTrue(WebDriver.InitializeWebDriver(out errorMessage), errorMessage);
        }

        [Given(@"I navigate to the Amazon Drive site")]
        public void GivenINavigateToTheAmazonDriveSite()
        {
            // Navigate to the website
            Assert.IsTrue(WebDriver.NavigateToWebSite(TestConstants.WebSiteURL,
                out errorMessage), errorMessage);

            // Check if the webpage is loaded
            Assert.IsTrue(initPage.AmIInInitialPage(out errorMessage),
                errorMessage);
        }

        [Given(@"I search for my book")]
        public void GivenISearchForMyBook()
        {
            // Search for the book
            Assert.IsTrue(initPage.InsertArticleName(TestConstants.BookName, out errorMessage),
                errorMessage);
        }

        [When(@"I select the cheaper book from the list")]
        public void GivenISelectTheCheaperBookFromTheList()
        {
            // Check if the list page is loaded
            Assert.IsTrue(listPage.AmIInArticleListPage(out errorMessage),
                errorMessage);

            // Check if the list page has books
            Assert.IsTrue(listPage.CheckIfListHasItems(out errorMessage),
                errorMessage);

            // Select the cheaper book from the list
            Assert.IsTrue(listPage.SelectCheaperArticle(TestConstants.BookName, out errorMessage),
                errorMessage);

            // Check if I am in the article page
            Assert.IsTrue(articlePage.AmIInArticlePage(out errorMessage),
                errorMessage);
        }

        [When(@"I select the book type")]
        public void WhenISelectTheBookType()
        {
            // Select 'Paperback' book type for adding it to the shopping cart
            Assert.IsTrue(articlePage.PickPaperbackType(out errorMessage),
                errorMessage);
        }

        [When(@"I add my book to the shopping cart")]
        public void WhenIAddMyBookToTheShoppingCart()
        {
            // Adding the book to the shopping card, clicking in the 'Add to cart' button
            Assert.IsTrue(articlePage.AddArticleToShoopingCart(out errorMessage),
                errorMessage);
        }


        [Then(@"my book must be added to the shipping cart")]
        public void ThenMyBookMustBeAddedToTheShippingCart()
        {
            // Check if the book is added to the shopping card
            Assert.IsTrue(articlePage.CheckArticleInShoopingCart(TestConstants.BookName, out errorMessage),
                errorMessage);
        }

        [AfterScenario()]
        public void TearDown()
        {
            // Take a screenshot of the last AT test and close the chrome driver
            WebDriver.TakeScreenshot();
            WebDriver.CloseWebDriver();
        }

    }
}
