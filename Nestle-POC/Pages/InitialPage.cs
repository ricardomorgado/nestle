﻿using Nestle_POC.Helpers;

namespace Nestle_POC.Pages
{

    class InitialPage
    {

        #region 'Initial' page private texts
        private const string PageTitle = "Amazon Cloud page";
        private const string SearchInput = "Search input field";
        private const string SearchButton = "Search button field";
        #endregion

        #region XPaths used in the 'Initial' page elements
        private const string lblSigninToolTip = "//*[@id='nav-signin-tooltip']";
        private const string txtSearchArea = "//*[@id='twotabsearchtextbox']";
        private const string btSearch = "//*[@class='nav-search-submit nav-sprite']";
        #endregion

        /// <summary>
        /// Check if the 'Amazon Cloud' is open
        /// </summary>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the page is not loaded; otherwise, returns true</returns>
        public bool AmIInInitialPage(out string errorMsg)
        {
            // Check if the 'login' label (last element loaded in the current page) inside of the 
            // 'Amazon Cloud' page is visible
            if (!SeleniumActions.WaitUntilWebElementIsVisible(lblSigninToolTip))
            {
                errorMsg = ErrorMessages.PageNavFail + PageTitle;
                return false;
            }
            errorMsg = null;
            return true;
        }

        /// <summary>
        /// Insert and search for an article name in the search area of the 'Amazon Cloud' page
        /// </summary>
        /// <param name="articleName">Name of the article to search</param>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the search process of the current article fails; otherwise, returns true</returns>
        public bool InsertArticleName(string articleName, out string errorMsg)
        {
            // Check if the 'Search' text input exists
            if (!SeleniumActions.SearchElement(txtSearchArea))
            {
                errorMsg = ErrorMessages.SearchElementInPageFail
                    .Replace("{ELEMENT}", SearchInput)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }

            // Insert the ArticleName in the text input field
            SeleniumActions.InsertTextInElement(txtSearchArea, articleName);

            // Click in the 'Search' button
            if (!SeleniumActions.ClickInElement(btSearch))
            {
                errorMsg = ErrorMessages.ClickElementInPageFail
                    .Replace("{ELEMENT}", SearchButton)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }
            errorMsg = null;
            return true;
        }

    }
}
