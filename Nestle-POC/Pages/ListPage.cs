﻿using Nestle_POC.Helpers;

namespace Nestle_POC.Pages
{
    class ListPage
    {

        #region 'List' page private texts
        private const string PageTitle = "Search article list page";
        private const string ShowResultsTitle = "Show results title description";
        private const string SortPriceCB = "Sort price object";
        private const string LowerPriceOptCB = "Lower price option";
        private const string SearchListResultLB = "Search list result";
        #endregion

        #region XPaths used in the 'List' page elements
        private const string txtShowResultsTitle = "//*[@id='leftNavContainer']//*[@class='a-size-medium a-spacing-base a-spacing-top-small a-color-tertiary a-text-normal']";
        private const string lstFirstResults = "//*[@id='s-results-list-atf']";
        private const string cbSearchForm = "//*[@id='searchSortForm']";
        private const string cbLowerPriceOpt = "//*[@id='sort']/option[2]";
        private const string lstResults = "//*[@id='s-results-list-atf']//li";
        #endregion

        /// <summary>
        /// Check if the search list page is loaded
        /// </summary>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the search list is not visible; otherwise, returns true</returns>
        public bool AmIInArticleListPage(out string errorMsg)
        {
            // Check if the 'search' list area is visible in the current page
            if (!SeleniumActions.SearchElement(txtShowResultsTitle))
            {
                errorMsg = ErrorMessages.PageNavFail + PageTitle;
                return false;
            }
            errorMsg = null;
            return true;
        }

        /// <summary>
        /// Check if the search list has items
        /// </summary>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the list is empty; otherwise, returns true</returns>
        public bool CheckIfListHasItems(out string errorMsg)
        {
            // Check if the summary label (visible in the top of the search list)
            // is visible
            // - If yes, the list contains items
            // - If no, the list is empty
            if (!SeleniumActions.SearchElement(lstFirstResults))
            {
                errorMsg = ErrorMessages.NoArticlesPageFail.Replace("{PAGE}", PageTitle);
                return false;
            }
            errorMsg = null;
            return true;
        }

        /// <summary>
        /// Select the cheaper article in the search list
        /// </summary>
        /// <param name="articleName">Name of the article</param>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the current process fails; otherwise, returns true</returns>
        public bool SelectCheaperArticle(string articleName, out string errorMsg)
        {
            // Sorting the articles by 'Price: Low to High'
            // - Clicking in the combobox
            if (!SeleniumActions.ClickInElement(cbSearchForm))
            {
                errorMsg = ErrorMessages.ClickElementInPageFail
                    .Replace("{ELEMENT}", SortPriceCB)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }

            // - Select 'Price: Low to High'
            if (!SeleniumActions.ClickInElement(cbLowerPriceOpt))
            {
                errorMsg = ErrorMessages.ClickElementInPageFail
                    .Replace("{ELEMENT}", LowerPriceOptCB)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }

            // Click in the book link in the sorted list of the page
            if (!SeleniumActions.SearchAndClickInArticleLink(lstResults, articleName))
            {
                errorMsg = ErrorMessages.ClickElementInPageFail
                    .Replace("{ELEMENT}", TestConstants.BookName)
                    .Replace("{PAGE}", SearchListResultLB);
                return false;
            }
            errorMsg = null;
            return true;
        }
    }
}
