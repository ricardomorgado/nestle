﻿using Nestle_POC.Helpers;

namespace Nestle_POC.Pages
{
    class ArticlePage
    {

        #region 'Article' page private texts
        private const string PageTitle = "Article page";
        private const string PaperbackType = "Paperback article option";
        private const string AddToCartButton = "Add to cart button";
        #endregion

        #region XPaths used in the 'Article' page elements
        private const string lblDeliveryArticleTypes = "//*[@id='mediaTabs_tabSetContainer']";
        private const string lblPaperback = "//*[contains(@class,'a-size-large mediaTab_title') and contains(text(),'Paperback')]";
        private const string btAddToCart = "//*[@id='add-to-cart-button-ubb']";
        private const string lblAddedClass = "//*[@id='huc-v2-order-row-icon']";
        #endregion

        /// <summary>
        /// Check if the 'Article' page is open
        /// </summary>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the page is not loaded; otherwise, returns true</returns>
        public bool AmIInArticlePage(out string errorMsg)
        {
            // Check if the 'delivery' article options label (last element loaded in the
            // current page) is visible
            if (!SeleniumActions.WaitUntilWebElementIsVisible(lblDeliveryArticleTypes))
            {
                errorMsg = ErrorMessages.PageNavFail + PageTitle;
                return false;
            }
            errorMsg = null;
            return true;
        }

        /// <summary>
        /// Pick the 'paperback' delivery article type
        /// </summary>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the current process fails; otherwise, returns true</returns>
        public bool PickPaperbackType(out string errorMsg)
        {
            // Check if the 'paperback' delivery option (most cheaper) is visible
            if (!SeleniumActions.WaitUntilWebElementIsVisible(lblPaperback))
            {
                errorMsg = ErrorMessages.SearchElementInPageFail
                    .Replace("{ELEMENT}", PaperbackType)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }

            // Pick the 'paperback' delivery option
            if (!SeleniumActions.ClickInElement(lblPaperback))
            {
                errorMsg = ErrorMessages.ClickElementInPageFail
                    .Replace("{ELEMENT}", PaperbackType)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }
            errorMsg = null;
            return true;

        }

        /// <summary>
        /// Add an article to the shopping card
        /// </summary>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>false if the current process fails; otherwise, returns true</returns>
        public bool AddArticleToShoopingCart(out string errorMsg)
        {
            // Search for the 'Add to cart' button
            if (!SeleniumActions.SearchElement(btAddToCart))
            {
                errorMsg = ErrorMessages.SearchElementInPageFail
                    .Replace("{ELEMENT}", AddToCartButton)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }

            // Click in the 'Add to cart' button
            if (!SeleniumActions.ClickInElement(btAddToCart))
            {
                errorMsg = ErrorMessages.ClickElementInPageFail
                    .Replace("{ELEMENT}", AddToCartButton)
                    .Replace("{PAGE}", PageTitle);
                return false;
            }

            errorMsg = null;
            return true;
        }

        /// <summary>
        /// Check if the article is in the shopping card
        /// </summary>
        /// <param name="articleName">Name of the article inserted in the shopping cart</param>
        /// <param name="errorMsg">Error message returned if an error occurs</param>
        /// <returns>true if the current article is added in the shopping card page; otherwise, returns false</returns>
        public bool CheckArticleInShoopingCart(string articleName, out string errorMsg)
        {
            // Check if the article is in the shopping card, validating
            // if the 'Added to Cart' object exists on the current page
            if (!SeleniumActions.SearchElement(lblAddedClass))
            {
                errorMsg = ErrorMessages.AddArticleToShoopingCardFail.Replace("{ART}", articleName);
                return false;
            }

            errorMsg = null;
            return true;

        }

    }
}
